const { saveFile, deleteFile } = require('../startup/fileReader')
var fs = require('fs'); 
var csv = require('csv-parser');
const path = require('path');

class fileReader {

    async readFile(req,res) {
        const filePath = req.files.csvFile
        await saveFile(filePath)
        const saveFilePath = path.join(__dirname, `../csvFiles/${req.files.csvFile.name}`);
        let processed = []
        let unProcessed = []
        await fs.createReadStream(saveFilePath)
            .pipe(csv())
            .on('data', (row) => {
                const isExist = processed.findIndex(x => x.mobile === row.mobile)
                if (isExist === -1) {
                    processed.push(row)
                } else {
                    unProcessed.push(row)
                }
            })
            .on('end', async () => {
                await deleteFile(saveFilePath)
                let finalData = {
                    processed: processed,
                    unProcessed: unProcessed
                }
                return res.status(200).send(finalData)
            })
    }
}


module.exports = fileReader