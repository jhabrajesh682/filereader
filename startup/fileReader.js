var fs = require('fs'); 
const path = require('path');


async function saveFile(incomingFile) {
    const saveFilePath = path.join(__dirname, `../csvFiles/${incomingFile.name}`);
    await incomingFile.mv(saveFilePath)
    return true
}

async function deleteFile(path) {
   await fs.unlink(path, (err)=> {
        if (err) return err
    })
    return true
}

module.exports = {
    saveFile,
    deleteFile
}